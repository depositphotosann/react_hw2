import styled from 'styled-components';
import PropTypes from 'prop-types';

const CartIcon = ({ initialCount }) => {
const [cartItemCount, setCartItemCount] = useState(initialCount);

const handleClick = () => {
  setCartItemCount(prevCount => prevCount + 1);
};
  
return (
  <IconContainer onClick={handleClick}>
    <i className="fas fa-shopping-cart"></i>
    <ItemCount>{cartItemCount}</ItemCount>
  </IconContainer>
);
};

CartIcon.propTypes = {
  initialCount: PropTypes.number.isRequired,
};

const IconContainer = styled.div`
  position: relative;
  cursor: pointer;
`;

const ItemCount = styled.span`
  position: absolute;
  top: -8px;
  right: -8px;
  background-color: red;
  color: white;
  border-radius: 50%;
  padding: 2px 6px;
  font-size: 12px;
`;

export default CartIcon;