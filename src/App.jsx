import './App.scss';
import MainComponent from './components/MainComponent/MainComponent';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    background-color: #f4f4f4;
  }
`;

const App = () => {
  return (
    <>
      <GlobalStyle />
      <MainComponent />
    </>
  );
};

export default App;

